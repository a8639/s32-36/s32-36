const express = require('express')
const router = express.Router()

const {
	create, 
	getAllCourses,
	getACourse,
	updateCourse,
	archive,
	unarchive,
	unArchive,
	isActive,
	activeCourses,
	deleteCourse

} = require('./../controllers/courseControllers')

const {verifyAdmin, verify} = require('./../auth')


//Create a route "/isActive" to get all active courses, return all documents found
	//both admin and regular user can access this route
router.get('/isActive', verify, async (req, res) => {
	try{
		await activeCourses().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route /create to create a new course, save the course in DB and return the document
	//Only admin can create a course
router.post('/create', verifyAdmin, async (req, res) => {
	// console.log(req.body)
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route "/" to get all the courses, return all documents found
	//both admin and regular user can access this route
router.get('/', verify, async (req, res) => {

	try{
		await getAllCourses().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})







//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
	//both admin and regular user can access this route
router.get('/:courseId', verify, async (req, res) => {
	try{
		await getACourse(req.params.courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//Create a route "/:courseId/update" to update course info, return the updated document
	//Only admin can update a course
router.put('/:courseId/update', verifyAdmin, async (req, res) => {
	try{
		await updateCourse(req.params.courseId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




//Create a route "/:courseId/archive" to update isActive to false, return updated document
	//Only admin can update a course
router.patch('/:courseId/archive', verifyAdmin, async (req, res) => {
	try{
		await archive(req.params.courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
	//Only admin can update a course

router.patch('/:courseId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unArchive(req.params.courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




//Create a route "/:id/delete-course" to delete a courses, return true if success
	//Only admin can delete a course
router.delete('/:courseId/delete-course', verifyAdmin, async (req, res) => {
	try{
		await deleteCourse(req.params.courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})





module.exports = router
