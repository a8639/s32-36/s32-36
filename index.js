const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

const PORT = process.env.PORT || 3007;
const app = express();

//connect our routes module
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
//prevents blocking of requests from client esp diff domains
app.use(cors())

//connect database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log (`Connected to database`));



//schema
    //schema is in models module


//routes
    //create a middleware to be the root url of all routes
    //default route is /api/users, any other routes that will be added (userRoutes)
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);




app.listen(PORT, () => console.log(`Server is connected to port ${PORT}`))
